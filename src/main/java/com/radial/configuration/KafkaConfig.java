package com.radial.configuration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;


/**
 * 
 * @author bselvam
 * KafkaConfig - This class is responsible for Kafka Produce Configuration. 
 */
@Configuration
@EnableKafka
public class KafkaConfig {

     @Value("${spring.kafka.bootstrap-servers}")
     private String bootstrapServers;

     @Bean
     public Map<String, Object> producerConfigs() {
          Map<String, Object> props = new HashMap<>();
          props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
          props.put(ProducerConfig.RETRIES_CONFIG, 0);
          props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
          props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
          props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16_384 * 4);
          props.put(ProducerConfig.LINGER_MS_CONFIG, 1);    
          //props.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
          props.put(ProducerConfig.ACKS_CONFIG, "1");
          return props;
     }
     
     
    

     @Bean
     public ProducerFactory<AtomicLong, String> producerFactory() {
          return new DefaultKafkaProducerFactory<>(producerConfigs());
     }

     @Bean
     public KafkaTemplate<AtomicLong, String> kafkaTemplate() {
          return new KafkaTemplate<>(producerFactory());
     }
     
     
     

}