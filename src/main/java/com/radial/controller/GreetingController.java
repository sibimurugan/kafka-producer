package com.radial.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.radial.model.Greeting;


@RestController
public class GreetingController {
	
	@Autowired
	private KafkaTemplate<AtomicLong, String> kafkaTemplate;

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	
	/**
	 * Just a simple get method consumes the message and drops into the kafka topic using producerRecord !! 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) throws Exception {
		ProducerRecord<AtomicLong, String> request = new ProducerRecord<>("kafkaTopicName", null, counter,name,addKafkaHeaders(counter, name));
		kafkaTemplate.send(request);
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}
	
	
	
	/**
	 * This methods helps in building the kafka Header
	 * @param counter
	 * @param name
	 * @return
	 * @throws Exception
	 */
	private List<Header> addKafkaHeaders(AtomicLong counter,String name) throws Exception {
		List<Header> headers = new ArrayList<>();
		headers.add(new RecordHeader("COUNTER", counter.toString().getBytes()));
		headers.add(new RecordHeader("NAME", name.getBytes()));
		return headers;
	}
}